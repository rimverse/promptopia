import Link from "next/link";

const Form = ({ type, post, setPost, submitting, handleSubmit }) => {


    return (
        <section className="w-full max-w-full flex justify-start items-start flex-col">
            <h1 className="text-left mt-5 text-5xl font-extrabold leading-[1.15] text-black sm:text-6xl">
                <span className="bg-gradient-to-r from-blue-600 to-cyan-600 bg-clip-text text-transparent">{type} Post</span></h1>
            <p className="mt-5 text-lg text-gray-600 sm:text-xl max-w-2xl text-left">
                {type} y compartir asombrosas indicaciones con el mundo y dejar volar tu imaginación con el poder de la IA
            </p>
            <form
                onSubmit={handleSubmit}
                className="mt-10 w-full max-w-2xl flex flex-col gap-7 rounded-xl border border-gray-200 bg-gray-600/10 shadow-[inset_10px_-50px_94px_0_rgb(199,199,199,0.2)] backdrop-blur p-5"
            >
                <label>
                    <span className="font-satoshi text-base font-semibold text-gray-700">
                        Tu indicacion IA
                    </span>
                    <textarea
                        value={post.prompt}
                        onChange={(e) => setPost({ ...post, prompt: e.target.value })}
                        placeholder="Escribe tu indicación..."
                        required
                        className="w-full flex rounded-lg h-[200px] mt-2 p-3 text-sm text-gray-500 outline-0"
                    >

                    </textarea>
                </label>
                <label>
                    <span className="font-satoshi text-base font-semibold text-gray-700">
                        Tag{` `}
                        <span className="font-normal">(#producto, #desarrolloweb, #idea)</span>
                    </span>
                    <input
                        value={post.tag}
                        onChange={(e) => setPost({ ...post, tag: e.target.value })}
                        placeholder="#tag"
                        required
                        className="w-full flex rounded-lg mt-2 p-3 text-sm text-gray-500 outline-0"
                    />

                </label>
                <div className="flex justify-end items-center mb-5 mx-3 gap-4">
                    <Link href='/' className="text-white text-sm font-semibold rounded-xl bg-red-700 px-5 py-1.5">
                        Cancelar
                    </Link>
                    <button
                        type="submit"
                        disabled={submitting}
                        className="px-5 py-1.5 text-sm font-semibold bg-primary-orange rounded-full text-white"
                    >
                        {submitting ? `${type}...` : type}
                    </button>
                </div>
            </form>
        </section>
    );
}

export default Form;
