'use client';

import Link from "next/link";
import Image from "next/image";
import { useState, useEffect } from "react";
import { signIn, signOut, useSession, getProviders } from 'next-auth/react';

export const Nav = () => {

    const { data: session } = useSession();

    const [providers, setProviders] = useState(null);
    const [toggleDropDown, setToggleDropDown] = useState(false);

    console.log(session?.user.image)
    const setUpProviders = async () => {
        const response = await getProviders();
        setProviders(response);
    };

    useEffect(() => {
        setUpProviders();
    }, [])

    return (
        <nav className="flex justify-between items-center w-full mb-16 pt-3">
            <Link href='/' className="flex gap-2 justify-center items-center ">
                <Image
                    src='/assets/images/logo.svg'
                    alt="Logo index"
                    width={30}
                    height={30}
                    className="object-contain"
                />
                <p className="max-sm:hidden font-satoshi font-semibold text-lg text-black tracking-wide">DiscoverPrompt</p>
            </Link>

            {/* Desktop */}
            <div className="sm:flex hidden">
                {session?.user ? (
                    <div className="flex gap-3 md:gap-5">
                        <Link href='/create-prompt' className="rounded-full border border-black bg-black py-1.5 px-5 text-white transition-all hover:bg-white hover:text-black text-center text-sm font-inter flex items-center justify-center">
                            Crear post
                        </Link>
                        <button
                            type="button"
                            onClick={signOut}
                            className="rounded-full border border-black bg-transparent py-1.5 px-5 text-black transition-all hover:bg-black hover:text-white text-center text-sm font-inter flex items-center justify-center"
                        >
                            Cerrar Sesión
                        </button>
                        <Link href='/profile'>
                            <Image
                                src={session?.user.image}
                                width={37}
                                height={37}
                                className="rounded-full"
                                alt="Profile"
                            />
                        </Link>
                    </div>
                ) : (
                    <>
                        {providers &&
                            Object.values(providers).map((provider) => (
                                <button
                                    type="button"
                                    key={provider.name}
                                    onClick={() => signIn(provider.id)}
                                    className="rounded-full border border-black bg-black py-1.5 px-5 text-white transition-all hover:bg-white hover:text-black text-center text-sm font-inter flex items-center justify-center"
                                >
                                    Iniciar Sesión
                                </button>
                            ))
                        }
                    </>
                )}
            </div>

            {/* Mobile */}

            <div className="sm:hidden flex relative">
                {session?.user ? (
                    <div className="flex">
                        <Image
                            src='/assets/images/logo.svg'
                            width={37}
                            height={37}
                            className="rounded-full"
                            alt="Profile"
                            onClick={() => setToggleDropDown((prev) => !prev)}
                        />
                        {toggleDropDown && (
                            <div className="absolute right-0 top-full mt-3 w-full p-5 rounded-lg bg-gray-200 min-w-[210px] flex flex-col gap-2 justify-end items-start">
                                <Link
                                    href='/profile'
                                    className="text-sm font-inter text-gray-700 hover:text-gray-500 font-medium"
                                    onClick={() => setToggleDropDown(false)}
                                >
                                    Mi perfil
                                </Link>
                                <Link
                                    href='/create-prompt'
                                    className="text-sm font-inter text-gray-700 hover:text-gray-500 font-medium"
                                    onClick={() => setToggleDropDown(false)}
                                >
                                    Crear prompt
                                </Link>
                                <button
                                    type="button"
                                    onClick={() => {
                                        setToggleDropDown(false);
                                        signOut();
                                    }}
                                    className="mt-5 w-full rounded-full border border-black bg-black py-1.5 px-5 text-white transition-all hover:bg-white hover:text-black text-center text-sm font-inter flex items-center justify-center"
                                >
                                    Cerrar Sesión
                                </button>
                            </div>
                        )}
                    </div>
                ) : (
                    <>
                        {providers &&
                            Object.values(providers).map((provider) => (
                                <button
                                    type="button"
                                    key={provider.name}
                                    onClick={() => signIn(provider.id)}
                                    className="rounded-full border border-black bg-black py-1.5 px-5 text-white transition-all hover:bg-white hover:text-black text-center text-sm font-inter flex items-center justify-center"
                                >
                                    Iniciar Sesión
                                </button>
                            ))
                        }
                    </>
                )}
            </div>
        </nav >
    )
};
