import { Schema, model, models } from "mongoose";

const UserSchema = new Schema({

    email: {
        type: String, unique: [true, 'El Email ya esta registrado'], required: [true, 'El Email es requerido']
    },
    username: {
        type: String, required: [true, 'El nombre de usuario es requerido'],
        match: [/^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/, "Nombre de usuario no válido, debe contener de entr 8 a 20 letras alfanumericas y ser único!"]
    },
    image: {
        type: String
    }
}, {
    versionKey: false
});

const User = models.User || model('User', UserSchema);

export default User;
