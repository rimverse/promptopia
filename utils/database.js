import mongoose from "mongoose";

let isConnected = false;

export const connectDb = async () => {

    mongoose.set('strictQuery', true);

    if (isConnected) {
        console.log('MongoDb esta corriendo correctamente!!');
        return;
    };

    try {
        await mongoose.connect(process.env.MONGODB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        isConnected = true;
        console.log('MongoDB conectado!!')
    } catch (error) {
        console.log(error);
    }
};