import { Nav } from '@components/Nav';
import { Provider } from '@components/Provider';
import '@styles/globals.css';

export const metadata = {
    title: 'Prompt blog',
    description: 'Discover & Share AI Prompts'
};

const RootLayout = ({ children }) => {
    return (
        <html lang='en'>
            <body>
                <Provider>
                    <div className='fixed w-[100vw] min-h-[100vh] flex justify-center pt-[120px] pb-[24px] pl-[160px] pr-[24px]'>
                        <div className='' />
                    </div>
                    <main className='relative z-10 flex justify-center items-center flex-col max-w-7xl mx-auto sm:px-16 px-6'>
                        <Nav />
                        {children}
                    </main>
                </Provider>
            </body>
        </html>
    );
}

export default RootLayout;
