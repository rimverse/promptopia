import Feed from "@components/Feed";

const Home = () => {
    return (
        <section className="w-full flex justify-center items-center flex-col">
            <h1 className="text-center mt-5 text-5xl font-extrabold leading-[1.15] text-black sm:text-6xl">Descubre & comparte
                <br className="max-md:hidden" />
                <span className="text-center bg-gradient-to-r from-amber-500 via-orange-600 to-yellow-500 bg-clip-text text-transparent">Indicaciones impulsadas por IA</span>
            </h1>
            <p className="text-center mt-5 text-lg text-gray-600 sm:text-xl max-w-2xl">
                Esta aplicación es una herramienta de código abierto impulsada por IA del mundo moderno para descubrir, crear y compartir indicaciones o prompts creativas.
            </p>

            {/* Feed */}
            <Feed />
        </section>
    );
}

export default Home;
