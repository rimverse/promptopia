import { connectDb } from "@utils/database";
import Prompt from "@models/prompt.model";

//GET (read)
export const GET = async (request, { params }) => {
    try {
        await connectDb();

        const prompt = await Prompt.findById(params.id).populate('creator').lean();

        if (!prompt) {
            return new Response('Prompt no encontrado', { status: 404 });
        };
        return new Response(JSON.stringify(prompt), { status: 200 });

    } catch (error) {
        return new Response('Error al cargar los archivos', { status: 500 });
    }
};

//PATCH(update)
export const PATCH = async (request, { params }) => {
    const { prompt, tag } = await request.json();

    try {
        await connectDb();

        const existePrompt = await Prompt.findById(params.id);

        if (!existePrompt) {
            return new Response('Prompt no encontrado', { status: 404 });
        };

        existePrompt.prompt = prompt;
        existePrompt.tag = tag;

        await existePrompt.save();

        return new Response(JSON.stringify(existePrompt), { status: 200 });

    } catch (error) {
        return new Response('Error al obtener el prompt', { status: 500 });
    }
};

//DELETE(delete)
export const DELETE = async (request, { params }) => {

    try {
        await connectDb();

        await Prompt.findByIdAndDelete(params.id);

        return new Response('Eliminado exitosamente!', { status: 200 });

    } catch (error) {
        return new Response('Error al obtener el prompt', { status: 500 });
    }
};