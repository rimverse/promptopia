import Prompt from "@models/prompt.model";
import { connectDb } from "@utils/database";

export const POST = async (req) => {
    const { userId, prompt, tag } = await req.json();

    try {
        await connectDb();

        const newPrompt = new Prompt({ creator: userId, tag, prompt });

        await newPrompt.save();

        return new Response(JSON.stringify(newPrompt), { status: 201 });

    } catch (error) {
        return new Response('Error al crear el prompt', { status: 500 })
    };
};