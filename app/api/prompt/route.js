import { connectDb } from "@utils/database";
import Prompt from "@models/prompt.model";


export const GET = async (request) => {
    try {
        await connectDb();
        const prompts = await Prompt.find({}).populate('creator').lean();
        return new Response(JSON.stringify(prompts), { status: 200 });

    } catch (error) {
        return new Response('Error al cargar los archivos', { status: 500 });
    }
};