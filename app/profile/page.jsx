'use client';

import { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";

import Profile from "@components/Profile";

const MiPerfil = () => {

    const router = useRouter();

    const { data: session } = useSession();
    const [posts, setPosts] = useState([])

    const handleEdit = (post) => {
        router.push(`/update-prompt?id=${post._id}`);
    };

    const handleDelete = async (post) => {
        const hasConfirm = confirm('Estas seguro de eliminar este post?');

        if (hasConfirm) {
            try {
                await fetch(`/api/prompt/${post._id.toString()}`, {
                    method: 'DELETE',
                });

                const filterPost = posts.filter((p) => p._id !== post._id);

                setPosts(filterPost);
            } catch (error) {
                console.log(error);
            }
        }
    };

    useEffect(() => {

        const fetchData = async () => {
            const response = await fetch(`/api/users/${session?.user.id}/posts`);
            const data = await response.json();
            setPosts(data);
        };

        if (session?.user.id) fetchData();
    }, [])

    return (
        <Profile
            name='Mi'
            desc='Bienvenido a tu página de perfil personalizada'
            data={posts}
            handleEdit={handleEdit}
            handleDelete={handleDelete}
        />
    );
}

export default MiPerfil;
